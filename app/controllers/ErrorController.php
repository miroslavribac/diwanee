<?php

namespace App\Controllers;

class ErrorController
{
    public function show()
    {
        echo 'Page not found!';
    }
}