<?php

namespace App\Controllers;

use App\Core\Controller;

class HomeController extends Controller
{
    public function index($id = '', $name='')
    {

        $this->view('home/index',[]);
        $this->view->page_title = 'This is home page';
        $this->view->render();
    }

    public function aboutUs()
    {
        $this->view('home/aboutUs',[]);
        $this->view->page_title = 'This is about page';
        $this->view->render();
    }
}