<?php

namespace App\Core;

class View
{
    protected $view_file;
    protected $data;
    public $page_title;

    public function __construct($view_file, $data)
    {
        $this->view_file = $view_file;
        $this->data = $data;
    }

    public function render()
    {
        if (file_exists(VIEW . $this->view_file . '.phtml')) {
            include(VIEW . $this->view_file . '.phtml');
        } else {
            echo 'File not exists';
        }
     }

     public function getAction()
     {
         return explode('/', $this->view_file)[1];
     }

}