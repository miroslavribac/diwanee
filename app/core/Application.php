<?php

namespace App\Core;

use App\Controllers\ErrorController;
use App\Controllers\HomeController;

class Application
{
    protected $controller = '';
    protected $action = '';
    protected $params = [];

    public function __construct()
    {
        $this->prepareURL();
    }

    private function prepareURL()
    {
        $request = trim($_SERVER['REQUEST_URI'], '/');

        if (!empty($request)) {
            $url = explode('/', $request);
            $this->controller = !empty($url[0]) ? $url[0] . 'Controller' : 'HomeController';
            $this->action = isset($url[1]) ? $url[1] : 'index';
            unset($url[0], $url[1]);
            $this->params = !empty($url) ? array_values($url) : [];
        }
        else {
            $this->controller = 'HomeController';
            $this->action = 'index';
            $this->params = [];
        }
    }

    public function run()
    {
        if (file_exists(CONTROLLER . $this->controller . '.php')) {

            $controller = "App\Controllers\\" . $this->controller;
            $this->controller = new $controller;
            if (method_exists($this->controller, $this->action)) {
                call_user_func_array([$this->controller, $this->action], $this->params);
            }
        } else {
            $this->controller = new ErrorController();
            $this->action = "show";
            call_user_func_array([$this->controller, $this->action], $this->params);
        }
    }
}