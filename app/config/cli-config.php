<?php

use Doctrine\ORM\Tools\Console\ConsoleRunner;

include_once CONFIG . 'bootstrap.php';

return ConsoleRunner::createHelperSet($em);