<?php

require_once ROOT . 'vendor/autoload.php';

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

$paths = array(MODEL);

$isDevMode = false;

// the connection configuration
$dbParams = array(
    'driver'   => 'pdo_mysql',
    'user'     => 'root',
    'password' => '',
    'dbname'   => 'diwanee',
);

$config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);
try {
    $em = EntityManager::create($dbParams, $config);
} catch (\Doctrine\ORM\ORMException $e) {
    echo $e->getMessage();
}
