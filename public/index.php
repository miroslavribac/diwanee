<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

define('ROOT', dirname(__DIR__) . DIRECTORY_SEPARATOR);
define('APP', ROOT . 'app' . DIRECTORY_SEPARATOR);
define('VIEW', ROOT . 'app' . DIRECTORY_SEPARATOR . 'view' . DIRECTORY_SEPARATOR);
define('MODEL', ROOT . 'app' . DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR);
define('CORE', ROOT . 'app' . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR);
define('DATA', ROOT . 'app' . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR);
define('CONTROLLER', ROOT . 'app' . DIRECTORY_SEPARATOR . 'controllers' . DIRECTORY_SEPARATOR);
define('CONFIG', ROOT . 'app' . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR);

include CONFIG . 'cli-config.php';

use App\Core\Application;

spl_autoload_register(
    function ($class_name) {
        if (file_exists(CORE. $class_name . '.php')) {
            require_once  CORE . $class_name . '.php';
        }
        if (file_exists(CONTROLLER. $class_name . '.php')) {
            require_once  CONTROLLER . $class_name . '.php';
        }
        if (file_exists(MODEL. $class_name . '.php')) {
            require_once  MODEL . $class_name . '.php';
        }
        if (file_exists(VIEW. $class_name . '.php')) {
            require_once  VIEW . $class_name . '.php';
        }
    }
);

$app = new Application();
$app->run();



